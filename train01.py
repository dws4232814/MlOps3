import datetime
import re
import string
import time

import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.nn import Embedding, Linear, Module, NLLLoss


def clean(
	inp: str,
) -> str:
	inp = inp.translate(
		str.maketrans(string.punctuation, " " * len(string.punctuation))
	)
	inp = re.sub(r"\s+", " ", inp.lower())
	return inp


def preprocess_text(
	data,
):
	return data.lower().split()


def create_skip_gram_pairs(
	tokens,
	window_size=2,
):
	pairs = []
	for i in range(window_size, len(tokens) - window_size):
		target = tokens[i]
		contexts = [
			tokens[j] for j in range(i - window_size, i + window_size + 1) if j != i
		]
		for context in contexts:
			pairs.append((target, context))
	return pairs


def build_vocab(tokens):
	vocab = set(tokens)
	word_to_ix = {word: i for i, word in enumerate(vocab)}
	return word_to_ix


class SkipGramModel(Module):
	def __init__(self, vocab_size, embedding_dim):
		super(SkipGramModel, self).__init__()
		self.embeddings = Embedding(
			num_embeddings=vocab_size, embedding_dim=embedding_dim
		)
		self.out = Linear(embedding_dim, vocab_size)

	def forward(
		self,
		tokens,
		word_to_ix,
		target_ix,
		window_size,
	):
		center_tensor = torch.tensor([target_ix], dtype=torch.long)
		embeds = self.embeddings(center_tensor)
		scores = self.out(embeds)
		log_probs = F.log_softmax(scores, dim=1)
		return log_probs


def train_core(
	model,
	word_to_ix,
	tokens,
	window_size,
	epochs,
	learning_rate,
):
	optimizer = optim.SGD(model.parameters(), lr=learning_rate)
	loss_function = NLLLoss()

	for epoch in range(epochs):
		total_loss = 0
		for pos in range(window_size, len(tokens) - window_size):
			# Convert words to indices
			for context_ix in range(pos - window_size, pos + window_size + 1):
				if context_ix != pos:
					model.zero_grad()
					# print(f"context_idx: {context_idx}")
					log_probs = model(tokens, word_to_ix, pos, window_size)
					loss = loss_function(log_probs, torch.tensor([context_ix]))
					loss.backward()
					optimizer.step()

					total_loss += loss.item()
		if (epoch + 1) % 10 == 0:
			print(f"{datetime.datetime.now()}: Epoch {epoch + 1}, Loss: {total_loss}")


def train(
	data: str,
):
	"""
	return: w2v_dict: dict
	        - key: string (word)
	        - value: np.array (embedding)
	"""
	# print(f"{datetime.datetime.now()}: start")
	tokens = preprocess_text(data)
	# print(f"{datetime.datetime.now()} preprocessing is done ")
	word_to_ix = build_vocab(tokens)
	# print(f"{datetime.datetime.now()} vocabular is created")

	vocab_size = len(word_to_ix)
	embedding_dim = 100
	model = SkipGramModel(vocab_size, embedding_dim)
	# print(f"{datetime.datetime.now()} model is created")
	train_core(model, word_to_ix, tokens, window_size=2, epochs=5, learning_rate=0.002)

	return {
		word: model.embeddings.weight[i].detach().numpy()
		for word, i in word_to_ix.items()
	}


with open("text.txt", "r", encoding="utf8") as file:
	text = clean(file.read())
	# text = clean('в лесу родилась елочка в лесу она росла queen man woman king')

startTime = time.time()
x = train(text)
print(f"Execution time: {time.time() - startTime}")
# n_embedding, window_size = 100, 5
# n_iter, learning_rate = 5, 0.002
