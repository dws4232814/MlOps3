# MlOps 3.0

## Add your files

- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dws4232814/MlOps3.git
git branch -M main
git push -uf origin main
```

The README file will be expanded as the project goes on.
