# Contributing

Before modifying the source code please follow the following instructions to setup linters and check your updates according to the project rules.

Install all necessary requirements by running `pip install -r requirements.txt`. The `ruff` linter will be installed among other dependencies. Please run `ruff check` to be sure that the changes match the project requirements. Anyway the changes will be checked on commit because of configured pre-commit hook in the repository. If your changes does match rules, commit will be rejected.
